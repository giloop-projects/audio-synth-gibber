// Grid for synth notes
var nbLines = 4;
var nbCols = 5;
var nbNotes = nbLines * nbCols;
var notesPlayed = [];
let colorNotes = [];
let xoff, yoff;
var notePlayed = undefined;

function setup() {
  cnv = createCanvas( windowWidth-5, windowHeight-5);
  cnv.mouseMoved(noteChanged); // attach listener for activity on canvas only

  xoff = windowWidth/nbCols;
  yoff = windowHeight/nbLines;

  // Mini preset : short, lead, winsome, bass, dark, dark2, easy, easyfx, noise
  synth = Mono('easy')
  .detune2.seq( Rndf(0,.015) )
  .detune3.seq( Rndf(0,.015) )
  
  //Synth2({ 
  //  maxVoices:4, amp:.5, resonance:4
   // useADSR: true, maxVoices:4, waveform:'Saw',
   // attack:ms(1), decay:ms(50), sustain:ms(2000), release:ms(50)
 // })
  // bass = FM('bass')
  //  .note.seq( [0,0,0,7,14,13].rnd(), [1/8,1/16].rnd(1/16,2) )
  // rhodes = Synth( 'rhodes', {amp:.35} )
  //  .chord.seq( Rndi(0,6,3), 1 )
  //  .fx.add( Delay() )

  Gibber.scale.root('c2');
  Gibber.scale.mode('Minor');
  // 
  
  stroke('white');
  strokeWeight(2);
  colorMode( HSB, 255 );
  // Couleur des notes 
  for (var n=0; n<nbNotes; n++) {
    colorNotes[n] = randomColor();
  }
}

function draw() {
  background( 64 );

  drawGridNotes();
  if (notePlayed>=0) {
    stroke('black');
    text('play:'+notePlayed, mouseX, mouseY);
  }
}

function drawGridNotes() {
 // Draw the grid of notes 
 
 
  for (var c = 0; c < nbCols; c++) {
    for (var l = 0; l < nbLines; l++) {
      n = l * (nbLines+1) + c;
      stroke('white');
      fill(colorNotes[n]);
      rect((xoff+1)*c, (yoff+1)*l, xoff, yoff);
      
    }
  }
 
}

function randomColor() {
  return color(random(0,128), random(0,128), 255);
}

function mousePressed() {
  notePlayed = floor(mouseY*nbLines/windowHeight) * (nbLines+1) + floor(mouseX*nbCols/windowWidth);
  synth.note(notePlayed,0.8);
  console.log('played:'+notePlayed)
}

function noteChanged() {

  if (notePlayed>=0) {
    newNote = floor(mouseY*nbLines/windowHeight) * (nbLines+1) + floor(mouseX*nbCols/windowWidth);
    console.log('new:'+newNote+', old:'+notePlayed)
    if (newNote != notePlayed) {
      notePlayed = newNote;
      synth.note(notePlayed,0);
      synth.note(newNote,0.8);
    }
  }
}
function mouseReleased() {
  synth.note(notePlayed,0);
  notePlayed = undefined;
}